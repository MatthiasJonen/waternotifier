"""Test functions measurement.py."""
from datetime import datetime

from waternotifier import measurement


def test_measurement_to_dict():
    """Make sure data is enriched completely with date and time."""
    assert measurement.measurement_to_dict("31", "%") == [
        {
            "date": datetime.now().date().strftime("%d.%m.%Y"),
            "time": datetime.now().time().strftime("%H:%M:%S"),
            "measurement_data": "31",
            "measurement_unit": "%",
        },
    ]
