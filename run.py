"""
Trigger the entire project.

Do this by "poetry run python run.py"
"""
import pypickle

from waternotifier import measurement


def run():
    """
    Run the program.

    Get current measurement and compare it to previous measurement.
    If changed, notify.
    """
    previous_measurement_file_path = "previous_measurement.pkl"
    previous_measurement = pypickle.load(previous_measurement_file_path)
    filling_percentage = measurement.read_serial_to_percentage()
    if filling_percentage != previous_measurement:
        pypickle.save(
            previous_measurement_file_path,
            filling_percentage,
            overwrite=True,
        )  # save measurement for next run
        pretty_printed = measurement.measurement_to_dict(filling_percentage, "%")
        measurement.write_data_file("datalog.json", "a", pretty_printed)
        measurement.send_to_element(
            "<h1>{percentage} %</h1>".format(percentage=filling_percentage),
        )
        measurement.sent_to_mqtt(filling_percentage)


if __name__ == "__main__":
    run()
