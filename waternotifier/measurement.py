"""Functions in the context of the actual measurement."""
import json
from datetime import datetime

import serial
import paho.mqtt.client as paho
from dotenv import dotenv_values
from matrix_client.client import MatrixClient

config = dotenv_values(
    ".env",
)  # config = {"ELEMENT_API_TOKEN": "some_token", "ELEMENT_ROOM_ID": "some_room_id"}


def read_serial_to_percentage():
    """Read serial port.

    Returns:
        filling percentage (str)

    Keyword Arguments:
        no

    Serial Structure
    b'#00 200109070231 400 398   990  55  3575  R-- rn'

    b'00          start indicator
    200109070231  date and time
    400           preasure
    398           temperature
    null
    990           filling high (mm)
    null
    55            filling percentage
    null
    3575          filling volume (l)
    null
    R--           relais
    rn            finish indicator

    """
    rate = 9600
    timeout_seconds = 30
    ser = serial.Serial(
        port="/dev/ttyS0",
        baudrate=rate,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=timeout_seconds,
    )
    raw_data_list = list(str(ser.readline()).split(" "))
    write_data_file("rawdata.json", "w", raw_data_list)
    if raw_data_list[8] == "":
        return raw_data_list[7]
    if raw_data_list[7] == "":
        return raw_data_list[6]


def measurement_to_dict(measurement_data: any, measurement_unit: any) -> dict:
    """Enrich measurement data with date and timestamp.

    Returns:
        dict

    Args:
        measurement_data (any): value to enrich
        measurement_unit (any): unit identifier e.g. %
    """
    return [
        {
            "date": datetime.now().date().strftime("%d.%m.%Y"),
            "time": datetime.now().time().strftime("%H:%M:%S"),
            "measurement_data": measurement_data,
            "measurement_unit": measurement_unit,
        },
    ]


def write_data_file(data_file, method, data_dict):
    """Write data to file.

    Args:
        data_file (str): filename e.g. data.json
        method (str): "a" for append, "w" for write new file
        data_dict (dict): data dictionary to write to file
    """
    with open(data_file, method) as write_file:
        json.dump(data_dict, write_file, indent=4)


def send_to_element(message):
    """Send data to element matrix client.

    Args:
        message (str): message to send
    """
    client = MatrixClient(config["ELEMENT_BASE_URL"])
    client.login(config["ELEMENT_USERNAME"], config["ELEMENT_PASSWORD"], sync=False)
    room = client.join_room(config["ELEMENT_ROOM_ID"])
    room.send_html(message)

def sent_to_mqtt(filling_percentage):
    """Send data to mqtt.

    Args:
        filling_percentage (str): value in %
    """
    client = paho.Client()
    client.username_pw_set(config["MQTT_USERNAME"], config["MQTT_PASSWORD"])
    if client.connect(config["MQTT_BROKER"], 1883, 60) != 0:
        print("Could not connect to MQTT Broker!")
        sys.exit(-1)
    client.publish("water/filling_percentage", filling_percentage)
    client.disconnect()
